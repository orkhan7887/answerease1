<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

/**
 * Shop Routes
 */
Route::group(
    [
        'middleware' => 'App\Http\Middleware\AuthenticateAccess::class',
        'namespace' => "App\Http\Controllers\Shop"
    ], function ($router) {

    Route::group(['prefix' => 'shops/categories'], function () {
        Route::post('list', 'ShopCategoryController@list');
        Route::post('create', 'ShopCategoryController@create');
    });

    Route::group(['prefix' => 'shops/products'], function () {
        Route::get('list', 'ShopProductController@list');
        Route::post('create', 'ShopProductController@create');
        Route::post('update', 'ShopProductController@update');
        Route::delete('delete', 'ShopProductController@softDelete');
        Route::get('search', 'ShopProductController@searchProducts');
    });

    Route::group(['prefix' => 'shops/orders'], function () {
        Route::get('list', 'ShopOrderController@listOrders');
        Route::get('show', 'ShopOrderController@showOrder');
        Route::post('edit', 'ShopOrderController@setOrderState');
    });
});

/**
 * User Routes
 */
Route::group(
    [
        'prefix' => 'users',
        'middleware' => 'App\Http\Middleware\AuthenticateAccess::class',
        'namespace' => "App\Http\Controllers\User"
    ], function ($router) {

    Route::group(['prefix' => 'products'], function () {
        Route::get('list', 'UserProductController@listProducts');
        Route::get('bundle', 'UserProductController@getBundleOfProducts');
        Route::post('show', 'UserProductController@showById');
        Route::get('viewhistory', 'UserProductController@incrementProductViewCount');
        Route::get('popular', 'UserProductController@getPopularProductList');
        Route::get('discount', 'UserProductController@getRecentlyDiscountedProductList');
        Route::get('similar', 'UserProductController@getSimilarProducts');
        Route::get('list_by_category', 'UserProductController@productListByCategory');
        Route::get('search', 'UserProductController@searchProducts');
        Route::get('show_cart', 'UserCartController@showMyCart');
        Route::get('count_cart', 'UserCartController@getCartItemCount');
        Route::post('add_cart', 'UserCartController@addProductToCart');
        Route::delete('remove_cart', 'UserCartController@removeProductFromCart');
        Route::get('reviews', 'UserReviewController@reviewsOfProduct');
        Route::post('add_review', 'UserReviewController@addReviewToProduct');
        Route::delete('remove_review', 'UserReviewController@removeReview');
        Route::get('like_review', 'UserReviewController@likeOrDislikeReview');
        Route::get('favorites', 'UserFavoriteProductController@getMyFavoritesList');
        Route::post('favorite', 'UserFavoriteProductController@addOrRemoveProductFavorites');
    });

    Route::group(['prefix' => 'categories'], function () {
        Route::get('list', 'UserCategoryController@list');
    });

    Route::group(['prefix' => 'orders'], function () {
        Route::get('list', 'UserOrderController@listOrders');
        Route::get('show', 'UserOrderController@showOrder');
        Route::post('create', 'UserOrderController@createOrder');
        Route::delete('remove', 'UserOrderController@removeOrder');
    });

    Route::group(['prefix' => 'payment'], function () {
        Route::get('pay', 'UserPaymentController@createPayment');
        Route::post('approve', 'UserPaymentController@approve');
        Route::post('cancel', 'UserPaymentController@cancel');
        Route::get('decline', 'UserPaymentController@decline');
    });
});



/**
 * General Routes
 */
Route::group(
    [
        'middleware' => 'App\Http\Middleware\AuthenticateAccess::class',
        'namespace' => "App\Http\Controllers"
    ], function ($router) {

    Route::group(['prefix' => 'general'], function () {
        Route::get('brand_list', 'GeneralController@brandList');
    });
});
