<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

// Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');


/**
 * User Routes
 */
Route::group(
    [
        'namespace' => "App\Http\Controllers\User"
    ], function ($router) {

    
    });
});



/**
 * General Routes
 */
Route::group(
    [
        'middleware' => 'App\Http\Middleware\AuthenticateAccess::class',
        'namespace' => "App\Http\Controllers"
    ], function ($router) {

    Route::post('register', 'UserController@register');
    
});
