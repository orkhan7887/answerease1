<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

// Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

Route::group(
    [
        'namespace' => "App\Http\Controllers"
    ], function ($router) {

    Route::post('register', 'UserController@register');
    

    Route::group(
        [
            'middleware' => 'auth:api',
            'namespace' => "App\Http\Controllers"
        ], function ($router) {

        
        Route::post('questions', 'QuestionController@addQuestion');
        Route::get('questions', 'QuestionController@getQuestion');
        Route::post('answers', 'AnswerController@Answer');

        
        
    });

    Route::post('questions', 'QuestionController@addQuestion');
    Route::get('questions', 'QuestionController@getQuestion');

    Route::post('answers', 'AnswerController@Answer');

    
    ['middleware' => ['auth:api']
});
