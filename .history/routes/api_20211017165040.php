<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

// Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

Route::group(
    [
        'namespace' => "App\Http\Controllers"
    ], function ($router) {

    Route::post('register', 'UserController@register');
    

    Route::middleware('auth:api')->get('/user', function (Request $request) {
        return $request->user()->id;
    });
    Route::post('questions', 'QuestionController@addQuestion');
    Route::get('questions', 'QuestionController@getQuestion');

    Route::post('answers', 'AnswerController@Answer');
    
});
