<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

// Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

Route::group(
    [
        'namespace' => "App\Http\Controllers"
    ], function ($router) {

    Route::post('register', 'UserController@register');
    Route::middleware('auth:api')->get('me', 'UserController@myDetails');
    Route::get('search', 'UserController@search');
    
    Route::middleware('auth:api')->post('questions', 'QuestionController@addQuestion');
    Route::get('questions', 'QuestionController@getQuestion');

    Route::post('answers', 'AnswerController@Answer');
    
});
