<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Question;

class Question extends Model
{
    use HasFactory;

    protected $fillable = [
        'text',
        'category_id'
    ];
}
