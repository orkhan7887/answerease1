<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Searchable\Searchable;
use Spatie\Searchable\SearchResult;

class Answer extends Model implements Searchable
{
    use HasFactory;

    protected $fillable = [
        'text',
        'question_id'
    ];

    public function getSearchResult(): SearchResult
    {
       $url = route('blogPost.show', $this->slug);
    
        return new \Spatie\Searchable\SearchResult(
           $this,
           $this->title,
           $url
        );
    }

}
