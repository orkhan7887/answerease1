<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\ProductDetail
 *
 * @property int $id
 * @property mixed $product_variation
 * @property mixed $specification_categories
 * @property int $product_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property float $weight
 * @property string $size
 * @property string $address
 * @property-read \App\Models\Product $product
 * @method static \Illuminate\Database\Eloquent\Builder|ProductDetail newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ProductDetail newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ProductDetail query()
 * @method static \Illuminate\Database\Eloquent\Builder|ProductDetail whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProductDetail whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProductDetail whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProductDetail whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProductDetail whereProductVariation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProductDetail whereSize($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProductDetail whereSpecificationCategories($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProductDetail whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProductDetail whereWeight($value)
 * @mixin \Eloquent
 */
class ProductDetail extends Model
{
    use HasFactory;

    protected $table = 'productdetails';

    protected $casts = [
        'prices' => 'array'
    ];
    protected $fillable = [
        'product_id',
        'variations',
        'variation_name',
        'specifications',
        'package_dimensions',
        'product_package_dimensions',
        'weight',
        'size',
        'address'
    ];

    protected $hidden = [
      'id',
      'product_id',
      'created_at',
      'updated_at',
      'address',
      'size'
    ];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}
