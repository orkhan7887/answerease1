<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Searchable\Searchable;
use Spatie\Searchable\SearchResult;

class Question extends Model
{
    use HasFactory;

    protected $fillable = [
        'text',
        'category_id',
        'point',
        'user_id'
    ];
}
