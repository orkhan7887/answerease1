<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Response;


class UserController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    function __construct() {
        //
    }

    
    public function register(Request $request)
    {
        try
        {
            $rules = [
                'name' => 'required|max:255',
                'surname' => 'required|max:255',
                'username' => 'required|unique:users',
                'password' => 'required|min:8',
            ];
            $this->validate($request, $rules);

            $fields = $request->all();
            $fields['password'] = \Hash::make($request->input('password'));

            $user = User::create($fields);
            $token = $user->createToken('My Token')->accessToken;
            $data = ['user' => $user, 'token' => $token];
            return $this->successResponse($data, Response::HTTP_CREATED);
        }
        catch (\Exception $e) {
            throw $e;
        }
    }

}