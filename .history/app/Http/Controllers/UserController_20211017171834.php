<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Response;
use App\Models\User;

class UserController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    function __construct() {
        //
    }

    
    public function register(Request $request)
    {
        try
        {
            $rules = [
                'name' => 'required|max:255',
                'surname' => 'required|max:255',
                'username' => 'required|unique:users',
                'password' => 'required|min:8',
            ];
            $this->validate($request, $rules);

            $fields = $request->all();
            $fields['password'] = \Hash::make($request->input('password'));

            $user = User::create($fields);
            $data = ['user' => $user];
            return response()->json($question, 201);
        }
        catch (\Exception $e) {
            throw $e;
        }
    }

    public function editProfile(Request $request)
    {
        try {
            $fields = $request->only('name', 'surname', 'description');
            if(empty($fields)) $this->errorResponse('Bad Request', RESPONSE::HTTP_BAD_REQUEST);

            $user = User::where('id', Auth::user()->id)->update($fields);
            if(!$user) $this->errorResponse("Couldn't Updated", Response::HTTP_BAD_REQUEST);
            return response()->json($user, 200);
        } catch (\Exception $e) {
            throw $e;
        }
    }

    public function editPicture(Request $request)
    {
        try {
            $rules = [
                'picture' => 'required|file',
            ];
            $this->validate($request, $rules);

            $path = $request->file('picture')->store('users');
            $fields['picture'] = $path;

            $user = User::where('id', Auth::user()->id)->update($fields);
            if(!$user) $this->errorResponse("Couldn't Updated", Response::HTTP_BAD_REQUEST);
            return response()->json($user, 200);
        } catch (\Exception $e) {
            throw $e;
        }
    }

    public function myDetails(Request $request)
    {
        try {
            $user = User::where('id', Auth::user()->id)->get();
            return response()->json($user, 200);
        } catch (\Exception $e) {
            throw $e;
        }
    }

    public function search(Request $request)
    {
        $searchResults = (new Search())
                            ->registerModel(User::class, 'name')
                            ->search('john');

    }

}