<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Response;
use App\Models\User;
use App\Models\Question;

class AnswerController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    function __construct() {
        //
    }

    
    public function addAnswer(Request $request)
    {
        try
        {
            $rules = [
                'text' => 'required|max:1000',
                'question_id' => 'required|integer',
            ];
            $this->validate($request, $rules);

            $fields = $request->all();
            $data = ['user_id' => Auth::user()->id];

            $user = Question::create($fields);
            return $this->successResponse($data, Response::HTTP_CREATED);
        }
        catch (\Exception $e) {
            throw $e;
        }
    }

}