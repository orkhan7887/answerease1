<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Response;
use App\Models\User;

class QuestionController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    function __construct() {
        //
    }

    
    public function addQuestion(Request $request)
    {
        try
        {
            $rules = [
                'text' => 'required|max:1000',
                'category_id' => 'required|integer',
            ];
            $this->validate($request, $rules);

            $fields = $request->all();

            $user = Question::create($fields);
            $data = ['user' => $user];
            return $this->successResponse($data, Response::HTTP_CREATED);
        }
        catch (\Exception $e) {
            throw $e;
        }
    }

}