<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Response;
use App\Models\User;
use App\Models\Question;

class QuestionController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    function __construct() {
        //
    }

    
    public function addQuestion(Request $request)
    {
        try
        {
            $rules = [
                'text' => 'required|max:1000',
                'category_id' => 'required|integer',
                'point' => 'required|integer'
            ];
            $this->validate($request, $rules);
            $fields = $request->all();
            $fields['user_id'] = auth()::user()->id;

            $data = Question::create($fields);
            return $this->successResponse($data, Response::HTTP_CREATED);
        }
        catch (\Exception $e) {
            throw $e;
        }
    }

    public function getQuestion(Request $request)
    {
        try
        {
            $question = Question::where('id', $request->input('id'));
            return $this->successResponse($question, Response::HTTP_CREATED);
        }
        catch (\Exception $e) {
            throw $e;
        }
    }

}