<?php

namespace App\Traits;

use Illuminate\Support\Facades\Storage;

Class Logs
{

    static function write($filename, $foldername, $content): bool
    {
        $logDate = date('d-M-Y');
        $logTime = date('H-i-s');
        $logFile = $filename.'-'.$logDate.'.log';
        $logText = '*** '.$logTime.' '.$logDate.' ***'."\r\n";

        if (is_array($content)) {
            $logText .= 'Array - '."\r\n";
            foreach ($content as $key => $value) {
                $logText .= $key.': '.json_encode($value)."\r\n";
            }
        } elseif (is_string($content)) {
            $logText .= 'String - '."\r\n";
            $logText .= $content."\r\n";

        } else {
            $content = json_encode($content);
            $logText .= 'Other - '."\r\n";
            $logText .= $content."\r\n";
        }
        $logText .= "============================================="."\r\n";
        return Storage::append($foldername.'/'.$logFile,$logText);
    }
}
